import antfu from '@antfu/eslint-config'

export default antfu(
  {
    rules: {
      'no-undef': 'off',
      'node/prefer-global/process': 'off',
    },
    ignores: [
      './dist/**/*',
      './node_modules/**/*',
      './.git/**/*',
      './.nuxt/**/*',
      './.idea/**/*',
    ],
  },
)
